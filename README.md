# How do I run the server? 

* Build the flask app image using

  ```
  docker build -t UOCIS-flask-demo .
  ```
  
* Run the container using
  
  ```
  docker run -d -p 5000:5000 UOCIS-flask-demo
  ```

* Launch http://0.0.0.0:5000/sample.html using web browser and check the output