from flask import *
import os

app = Flask(__name__)

# req = request.path
# print("Request is:", req)

@app.errorhandler(404)
def error_404(e):
    return render_template('404.html'), 404


@app.errorhandler(403)
def error_403(e):
	return render_template('403.html'), 403


@app.route("/<path:page>")
def check_page(page):

	extension = page.split(".")[-1] # Grabbing the extension
	url_delim = page.split("/")
	print("---->", request.path)
	print("Requested page is:", page)
	print("Requested extension is:", extension)

	if len(url_delim) > 2 and url_delim[-2] == "":
		abort(403)
	elif page[0] == "~" or page[0:2] == "..":
		abort(403)
	elif extension != "html" and extension != "css":
		abort(403)
	elif not os.path.exists('./templates/' + page):
		abort(404)
	else:
		return render_template(page)


if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
